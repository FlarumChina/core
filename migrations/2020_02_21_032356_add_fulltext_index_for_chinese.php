<?php

use Illuminate\Database\Schema\Builder;

return [
    'up' => function (Builder $schema) {
        $connection = $schema->getConnection();
        $prefix = $connection->getTablePrefix();
        $connection->statement('ALTER TABLE ' . $prefix . 'posts DROP INDEX content');
        $connection->statement('CREATE FULLTEXT INDEX content ON `' . $prefix . 'posts` (`content`) WITH PARSER ngram');
        $connection->statement('ALTER TABLE ' . $prefix . 'discussions DROP INDEX title');
        $connection->statement('CREATE FULLTEXT INDEX title ON `' . $prefix . 'discussions` (`title`) WITH PARSER ngram');
    },

    'down' => function (Builder $schema) {
        $connection = $schema->getConnection();
        $prefix = $connection->getTablePrefix();
        $connection->statement('ALTER TABLE ' . $prefix . 'posts DROP INDEX content');
        $connection->statement('ALTER TABLE ' . $prefix . 'discussions DROP INDEX title');
    }
];
