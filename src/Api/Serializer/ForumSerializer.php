<?php

/*
 * This file is part of Flarum.
 *
 * For detailed copyright and license information, please view the
 * LICENSE file that was distributed with this source code.
 */

namespace Flarum\Api\Serializer;

use Flarum\Foundation\Application;
use Flarum\Http\UrlGenerator;
use Flarum\Settings\SettingsRepositoryInterface;

class ForumSerializer extends AbstractSerializer
{
    /**
     * {@inheritdoc}
     */
    protected $type = 'forums';

    /**
     * @var Application
     */
    protected $app;

    /**
     * @var SettingsRepositoryInterface
     */
    protected $settings;

    /**
     * @var UrlGenerator
     */
    protected $url;

    /**
     * @param Application $app
     * @param SettingsRepositoryInterface $settings
     * @param UrlGenerator $url
     */
    public function __construct(Application $app, SettingsRepositoryInterface $settings, UrlGenerator $url)
    {
        $this->app = $app;
        $this->settings = $settings;
        $this->url = $url;
    }

    /**
     * {@inheritdoc}
     */
    public function getId($model)
    {
        return 1;
    }

    /**
     * {@inheritdoc}
     */
    protected function getDefaultAttributes($model)
    {
        $attributes = [
            'title' => $this->settings->get('forum_title'),
            'description' => $this->settings->get('forum_description'),
            'showLanguageSelector' => (bool)$this->settings->get('show_language_selector'),
            'baseUrl' => $url = $this->app->url(),
            'basePath' => parse_url($url, PHP_URL_PATH) ?: '',
            'debug' => $this->app->inDebugMode(),
            'apiUrl' => $this->app->url('api'),
            'welcomeTitle' => $this->settings->get('welcome_title'),
            'welcomeMessage' => $this->settings->get('welcome_message'),
            'themePrimaryColor' => $this->settings->get('theme_primary_color'),
            'themeSecondaryColor' => $this->settings->get('theme_secondary_color'),
            'logoUrl' => $this->getLogoUrl(),
            'faviconUrl' => $this->getFaviconUrl(),
            'headerHtml' => $this->settings->get('custom_header'),
            'footerHtml' => $this->settings->get('custom_footer'),
            'allowSignUp' => (bool)$this->settings->get('allow_sign_up'),
            'isCrawler' => $this->isCrawler(),
            'defaultRoute' => $this->settings->get('default_route'),
            'canViewDiscussions' => $this->actor->can('viewDiscussions'),
            'canStartDiscussion' => $this->actor->can('startDiscussion'),
            'canViewUserList' => $this->actor->can('viewUserList')
        ];

        if ($this->actor->can('administrate')) {
            $attributes['adminUrl'] = $this->app->url('admin');
            $attributes['version'] = $this->app->version();
        }

        return $attributes;
    }

    /**
     * @return \Tobscure\JsonApi\Relationship
     */
    protected function groups($model)
    {
        return $this->hasMany($model, GroupSerializer::class);
    }

    /**
     * @return null|string
     */
    protected function getLogoUrl()
    {
        $logoPath = $this->settings->get('logo_path');

        return $logoPath ? $this->app->url('assets') .'/'. $logoPath : null;
    }

    /**
     * @return bool
     */
    function isCrawler() {
        $agent= strtolower($_SERVER['HTTP_USER_AGENT']);
        if (!empty($agent)) {
            $spiderSite= array(
                "Bytespider",
                "360Spider",
                "Alexa (IA Archiver)",
                "Ask",
                "BaiduGame",
                "BaiDuSpider",
                "Baiduspider+",
                "Baiduspider-image",
                "BSpider",
                "bingbot",
                "ChinasoSpider",
                "Custo",
                "EasouSpider",
                "Exabot",
                "Fish search",
                "Google AdSense",
                "Googlebot",
                "Heritrix",
                "ia_archiver",
                "Java (Often spam bot)",
                "larbin",
                "legs",
                "lwp-trivial",
                "MJ12bot",
                "MSIECrawler",
                "msnbot",
                "MSNBot",
                "Netcraft",
                "Nutch",
                "OutfoxBot/YodaoBot",
                "Perl tool",
                "Python-urllib",
                "Sogou blog",
                "Sogou inst spider",
                "Sogou News Spider",
                "Sogou Orion spider",
                "Sogou Spider",
                "Sogou spider2",
                "Sogou web spider",
                "Sosospider",
                "Sosospider+",
                "Speedy Spider",
                "StackRambler",
                "SurveyBot",
                "TencentTraveler",
                "The web archive (IA Archiver)",
                "twiceler",
                "Voila",
                "WGet tools",
                "yacy",
                "Yahoo Slurp",
                "Yahoo! Slurp",
                "Yandex bot",
                "Yisouspider",
                "YoudaoBot",
            );
            foreach($spiderSite as $val) {
                $str = strtolower($val);
                if (strpos($agent, $str) !== false) {
                    return true;
                }
            }
            return false;
        } else {
            return false;
        }
    }

    /**
     * @return null|string
     */
    protected function getFaviconUrl()
    {
        $faviconPath = $this->settings->get('favicon_path');

        return $faviconPath ? $this->app->url('assets') . '/' . $faviconPath : null;
    }
}
