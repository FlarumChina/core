<?php

/*
 * This file is part of Flarum.
 *
 * For detailed copyright and license information, please view the
 * LICENSE file that was distributed with this source code.
 */

namespace Flarum\Install\Steps;

use Flarum\Foundation\Application;
use Flarum\Install\Step;
use Flarum\Settings\DatabaseSettingsRepository;
use Illuminate\Database\ConnectionInterface;

class WriteSettings implements Step
{
    /**
     * @var ConnectionInterface
     */
    private $database;

    /**
     * @var array
     */
    private $custom;

    public function __construct(ConnectionInterface $database, array $custom)
    {
        $this->database = $database;
        $this->custom = $custom;
    }

    public function getMessage()
    {
        return 'Writing default settings';
    }

    public function run()
    {
        $repo = new DatabaseSettingsRepository($this->database);

        $repo->set('version', Application::VERSION);

        foreach ($this->getSettings() as $key => $value) {
            $repo->set($key, $value);
        }
    }

    private function getSettings()
    {
        return $this->custom + $this->getDefaults();
    }

    private function getDefaults()
    {
        return [
            'allow_post_editing' => 'reply',
            'allow_renaming' => '10',
            'allow_sign_up' => '1',
            'custom_less' => '',
            'default_locale' => 'zh_Hans',
            'default_route' => '/all',
            'extensions_enabled' => '[]',
            'forum_title' => 'A new Flarum forum',
            'forum_description' => '',
            'mail_driver' => 'log',
            'mail_from' => 'noreply@localhost',
            'theme_colored_header' => '1',
            'theme_dark_mode' => '0',
            'theme_primary_color' => '#099cec',
            'theme_secondary_color' => '#3ab0f0',
            'welcome_message' => '这是 Flarum 的中文优化版, 如果遇到了问题, 你可以访问我们的 <a href="https://flarum.atowerlight.cn">支持社区</a> 以获得帮助',
            'welcome_title' => '欢迎使用 FlarumChina',
        ];
    }
}
