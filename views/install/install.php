<h2>安装 FlarumChina</h2>

<p>请按照下面的类容进行填写，以完成 FlarumChina 的安装。如果遇到问题，请到 <a href="https://flarum.atowerlight.cn/" target="_blank">FlarumChina 网站</a> 寻求答案。</p>

<form method="post">
  <div id="error" style="display:none"></div>

  <div class="FormGroup">
    <div class="FormField">
      <label>网站标题</label>
      <input name="forumTitle">
    </div>
  </div>

  <div class="FormGroup">
    <div class="FormField">
      <label>MySQL 地址</label>
      <input name="mysqlHost" value="localhost">
    </div>

    <div class="FormField">
      <label>MySQL 数据库</label>
      <input name="mysqlDatabase">
    </div>

    <div class="FormField">
      <label>MySQL 用户名</label>
      <input name="mysqlUsername">
    </div>

    <div class="FormField">
      <label>MySQL 密码</label>
      <input type="password" name="mysqlPassword">
    </div>

    <div class="FormField">
      <label>数据库表前缀</label>
      <input type="text" name="tablePrefix">
    </div>
  </div>

  <div class="FormGroup">
    <div class="FormField">
      <label>管理员用户名</label>
      <input name="adminUsername">
    </div>

    <div class="FormField">
      <label>管理员 Email</label>
      <input name="adminEmail">
    </div>

    <div class="FormField">
      <label>管理员密码</label>
      <input type="password" name="adminPassword">
    </div>

    <div class="FormField">
      <label>确认管理员</label>
      <input type="password" name="adminPasswordConfirmation">
    </div>
  </div>

  <div class="FormButtons">
    <button type="submit">点击安装</button>
  </div>
</form>

<script src="https://cdn.jsdelivr.net/npm/jquery@2.1.4/dist/jquery.min.js"></script>
<script>
$(function() {
  $('form :input:first').select();

  $('form').on('submit', function(e) {
    e.preventDefault();

    var $button = $(this).find('button')
      .text('请稍后...')
      .prop('disabled', true);

    $.post('', $(this).serialize())
      .done(function() {
        window.location.reload();
      })
      .fail(function(data) {
        $('#error').show().text('遇到了一些问题:\n\n' + data.responseText);

        $button.prop('disabled', false).text('点击安装');
      });

    return false;
  });
});
</script>
