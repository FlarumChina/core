<h2>遇到了一些问题</h2>

<p>您必须在安装之前解决这些问题。如果遇到问题，请到 <a href="https://flarum.atowerlight.cn/" target="_blank">FlarumChina 网站</a> 寻求答案。</p>

<div class="Problems">
  <?php foreach ($problems as $problem) { ?>
    <div class="Problem">
      <h3 class="Problem-message"><?php echo $problem['message']; ?></h3>
      <?php if (! empty($problem['detail'])) { ?>
        <p class="Problem-detail"><?php echo $problem['detail']; ?></p>
      <?php } ?>
    </div>
  <?php } ?>
</div>
