<h2>升级 FlarumChina</h2>

<p>在升级之前您需要 <strong>备份数据库</strong>. 如果遇到问题，请到 <a href="https://flarum.atowerlight.cn/" target="_blank">FlarumChina 网站</a> 寻求答案。</p>

<form method="post">
  <div id="error" style="display:none"></div>

  <div class="FormGroup">
    <div class="FormField">
      <label>数据库密码</label>
      <input type="password" name="databasePassword">
    </div>
  </div>

  <div class="FormButtons">
    <button type="submit">点击升级</button>
  </div>
</form>

<script src="https://cdn.jsdelivr.net/npm/jquery@2.1.4/dist/jquery.min.js"></script>
<script>
$(function() {
  $('form :input:first').select();

  $('form').on('submit', function(e) {
    e.preventDefault();

    var $button = $(this).find('button')
      .text('请稍后')
      .prop('disabled', true);

    $.post('', $(this).serialize())
      .done(function() {
        window.location.reload();
      })
      .fail(function(data) {
        $('#error').show().text('遇到了一些问题:\n\n' + data.responseText);

        $button.prop('disabled', false).text('点击升级');
      });

    return false;
  });
});
</script>
